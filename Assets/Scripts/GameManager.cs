﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private const int maxRange = 20;
    [Range(1, maxRange)]
    public int xSize = 1;
    [Range(1, maxRange)]
    public int ySize = 1; // Q - should this be called zSize?
    [Range(1, maxRange)]
    public float speed = 3;
    [Range(1, maxRange)]
    public float amplitude = 5;

    private int maxVerticesXDirection;
    private int maxVerticesYDirection;


    private Mesh mesh;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private MeshCollider meshCollider;

    private Vector3[] vertices;
    private int[] triangles;
    private Vector2[] uv;
    private Vector4[] tangents;
    Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

    private void Awake()
    {
        Generate();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void Generate()
    {
        const int numVerticesPerTriangle = 3;
        const int numTrianglesPerQuad = 2;
        const int numOfVerticesPerQuad = numVerticesPerTriangle * numTrianglesPerQuad;
        
        int maxVerticesXDirection = xSize + 1;
        int maxVerticesYDirection = ySize + 1;
        vertices = new Vector3[maxVerticesXDirection * maxVerticesYDirection]; // plus one for an additional point so we have the right ammount of grids
        triangles = new int[maxVerticesXDirection * maxVerticesYDirection * numOfVerticesPerQuad];

        uv = new Vector2[vertices.Length];

        tangents = new Vector4[vertices.Length];
        

        meshFilter = gameObject.AddComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        mesh = GetComponent<MeshFilter>().mesh = new Mesh();
        mesh.name = "Procedural Mesh";
        meshRenderer.material = Resources.Load("Assets/Materials/Grid_Material.mat") as Material;

        
        /* Generate Vertices */
        for (int y = 0, vertexIdx = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, vertexIdx++){ 
                vertices[vertexIdx] = new Vector3(x, Mathf.Sin(vertexIdx), y);
                uv[vertexIdx] = new Vector2((float)x / xSize, (float)y / ySize);
                tangents[vertexIdx] = tangent;
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.tangents = tangents;

        /* Generate Triangles */
        for (int y = 0, vertexIdx = 0, triangleIdx = 0; y < ySize; y++, vertexIdx++, triangleIdx += 6)
        {
            for (int x = 0; x < xSize; x++, vertexIdx++, triangleIdx += 6)
            {
                triangles[triangleIdx] = vertexIdx;
                triangles[triangleIdx + 1] = triangles[triangleIdx + 4] = vertexIdx + maxVerticesXDirection;
                triangles[triangleIdx + 2] = triangles[triangleIdx + 3] = vertexIdx + 1;
                triangles[triangleIdx + 5] = vertexIdx + maxVerticesXDirection + 1;
            }
        }
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
        meshCollider = gameObject.AddComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh;
        meshCollider.convex = false;
    }
    private void OnDrawGizmos()
    {
        // if vertices have not been initialized yet, prevents error message in editor
        if (vertices == null)
        {
            return;
        }
        Gizmos.color = Color.black;
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], 0.1f);
        }
    }
    private void FixedUpdate()
    {
        /* Creation and Assigning of Vertices */
        for (int y = 0, vertexIdx = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, vertexIdx++)
            {
                vertices[vertexIdx] = new Vector3(x, 3.0f * Mathf.Cos((Time.time / 10f) * (y/Mathf.PI) + (x/Mathf.PI)) ,y);
                uv[vertexIdx] = new Vector2((float)x / xSize, (float) y/ ySize);
                tangents[vertexIdx] = tangent;
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.tangents = tangents;
        mesh.RecalculateNormals();
        meshCollider.sharedMesh = mesh;

        ///* Generate Triangles */
        //for (int y = 0, vertexIdx = 0, triangleIdx = 0; y < ySize; y++, vertexIdx++, triangleIdx += 6)
        //{
        //    for (int x = 0; x < xSize; x++, vertexIdx++, triangleIdx += 6)
        //    {
        //        triangles[triangleIdx] = vertexIdx;
        //        triangles[triangleIdx + 1] = triangles[triangleIdx + 4] = vertexIdx + maxVerticesXDirection;
        //        triangles[triangleIdx + 2] = triangles[triangleIdx + 3] = vertexIdx + 1;
        //        triangles[triangleIdx + 5] = vertexIdx + maxVerticesXDirection + 1;
        //    }
        //}
        //mesh.triangles = triangles;
        //mesh.RecalculateNormals();

    }
}
